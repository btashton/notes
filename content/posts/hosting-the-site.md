---
title: "Hosting the Site"
date: 2018-10-23T15:59:11-07:00
draft: false
---

# Setting Up This Site With Netlify

I have used GitHub and GitLab pages before, but in my experience  I always have to do more configuration than I want to do, and eventually I just stop writing any content. A few weeks back I came across Netlify as a solution that works out of the box with Jekyll and Hugo among others.  It also has a bunch of other functionality for building things like forms that I do not need, but I could see that being useful another time.

There is not much more to say here besides I followed the tutorial at https://www.netlify.com/blog/2016/09/21/a-step-by-step-guide-victor-hugo-on-netlify/  and a short while later the site was live.

Anyway if you have come across this page than it must be doing its job.

* https://netlify.com
* https://gohugo.io