---
title: "Nuttx QEMU x86 Part 1"
date: 2018-10-23T16:58:14-07:00
draft: false
---

# Nuttx

About 5 years back at a previous job I discovered the Nuttx project and it has the ambitious goal of trying to provide as close to a POSIX like OS for embedded applications.  The vast majority of the code has been written by Greg Nutt, but in the last couple years others have been making considerable contributions especially for supporting more boards and chips.

One of the areas that I have wanted to be contributing more to is adding support for more devices, especially USB ones.  It would also be nice to be able to test more features without having to rely on physical embedded hardware. There is support for running in a simulator, but it ends up being rather limited and adding emulated devices is hard.

http://nuttx.org/doku.php?id=wiki:nxinternal:simulator

There is really no reason we should not be able to run a Nuttx Kernel as a guest OS under KVM. If I can get this working I hope that it will help reduce the bar for developing features for this OS.

## Current State of Things

There is a port to support i486 with QEMU, but it does not seem to build correctly, at least on my machine, giving the error:
 >`cc1: error: CPU you selected does not support x86-64 instruction set`

There is also active work going on to support x86_64 by a researcher in Taiwan who is trying to get Nuttx running as a guest OS for the Jailroot project, but that code will not be available until after his paper is published which is a ways out.

In the short term there is no real reason that we need to support x86_64 right away, and in some ways 32-bit mode is actually easier to support.

## First Priorities
* Get the Nuttx Shell project running again in the QEMU emulator
* Implement the base Virtio guest drivers

While I would like to have USB on the list, that is going to take some serious effort.  The primarily supported emulated USB host is XHCI which there is no support for in Nuttx right now.  EHCI is supported in both, but I would rather put the effort toward the long term path.

https://github.com/qemu/qemu/blob/master/docs/usb2.txt


## Virtio

* Highlights: https://wiki.osdev.org/Virtio
* Specification: http://docs.oasis-open.org/virtio/virtio/v1.0/cs04/virtio-v1.0-cs04.html
* Reference code: http://www.dumais.io/index.php?article=aca38a9a2b065b24dfa1dee728062a12

Likely will only support Virtio PCI since the MMIO interface has largely be abandoned, and there is really not good reason to be supporting it for new development.
